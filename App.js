import React, { useRef } from 'react';
import { StatusBar } from 'expo-status-bar';
import { WebView } from 'react-native-webview';
import * as WebBrowser from 'expo-web-browser';
import Popover from 'react-native-popover-view';
import { MaterialIcons } from '@expo/vector-icons';
import { SafeAreaView, Appearance, View, Text, Image, TouchableOpacity } from 'react-native';

const App = () => {
  const ref = useRef(null);
  const isDark = Appearance.getColorScheme() === 'dark';
  const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Safari/605.1.15';

  const handleLoad = () => {
    ref.current.injectJavaScript(`
      const init = () => {
        const head = document.head || document.getElementsByTagName('head')[0];
        const style = document.createElement('style');
        const css = '.landing-wrapper { min-width: 100vw; } .two { min-width: 100vw; } .two > div:last-child { flex: 0; } .two > div:nth-last-child(number) { flex: 100%; } #side > header > div:last-child > div > span > div:first-child { display: none; } .two > div:nth-child(2) > div:first-child { flex: 100%; } .app-wrapper-web > div:first-child .copyable-area { min-width: 100vw; } .app-wrapper-web > div:first-child:not(.main) .copyable-area > div { min-width: 80vw; } .app-wrapper-web > span div.copyable-area { min-width: 100vw; } .app-wrapper-web > span div.copyable-area > div { min-width: 80vw; } .app-wrapper-web { max-width: 100vw; } .app-wrapper-web > div.three > div:nth-child(2) { max-width: 100vw; } .three { min-width: auto; max-width: 100vw; } *::-webkit-scrollbar { display: none; } * { -ms-overflow-style: none; scrollbar-width: none; }';
        head.appendChild(style);

        if (style.styleSheet) style.styleSheet.cssText = css
        else style.appendChild(document.createTextNode(css));

        document.addEventListener('message', (deter) => {
          const field = document.createElement('input');
          field.setAttribute('type', 'text');
          document.body.appendChild(field);

          setTimeout(() => {
            field.focus();
            setTimeout(() => field.setAttribute('style', 'display:none;'), 50);
          }, 50);

          handleToggleChatroom(false);
        });
      };

      const detectWhatsappLogin = () => {
        const interval = setInterval(() => {
          if (document.getElementsByClassName('two')[0]) {
              clearInterval(interval);
              setTimeout(handleWhatsappMobile, 800);
          }
        }, 1300);
      };

      const handleToggleChatroom = (deter = true) => {
        const main = document.getElementsByClassName('two')[0].children;
        const leftPane = main[2];
        const rightPane = main[3];

        leftPane.style.flex = deter ? '0%' : '100%';
        rightPane.style.flex = deter ? '100%' : '0%';
        if (deter) window.ReactNativeWebview.postMessage('chatroom');
      };

      const handleWhatsappMobile = () => {
        const sideBar = document.getElementById('pane-side');
        const people = sideBar.children[0].children[0].children[0].children
        for (let key in people) {
          people[key].addEventListener('click', handleToggleChatroom);
        }
      };

      const handleDarkTheme = () => {
        if (${String(isDark)}) document.body.classList.add('dark');
        else document.body.classList.remove('dark');
      };

      init();
      handleDarkTheme();
      detectWhatsappLogin();
    `);
  };

  const handleRefresh = () => {
    ref.current.reload();
  };

  const handleContact = async () => {
    await WebBrowser.openBrowserAsync('http://isak.hk/');
  };

  const handleDonate = async () => {
    await WebBrowser.openBrowserAsync('https://www.buymeacoffee.com/5kahoisaac');
  };

  return (
    <SafeAreaView style={{ flex: 1, marginTop: 30, backgroundColor: isDark ? '#2a3032' : '#ededed' }}>
      <StatusBar style='auto' />
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          paddingVertical: 5,
          paddingHorizontal: 16,
          backgroundColor: isDark ? '#2a3032' : '#ededed'
        }}
      >
        <TouchableOpacity style={{ padding: 3.5 }} onPress={() => ref.current.postMessage(false)}>
          <MaterialIcons name='keyboard-arrow-left' size={30} color={isDark ? '#b0b3b5' : '#919191'} />
        </TouchableOpacity>
        <View style={{ flex: 1 }} />
        <TouchableOpacity style={{ padding: 6 }} onPress={handleRefresh}>
          <MaterialIcons name='refresh' size={25} color={isDark ? '#b0b3b5' : '#919191'} />
        </TouchableOpacity>
        <Popover
          from={(
            <TouchableOpacity style={{ padding: 6, marginLeft: 2 }}>
              <MaterialIcons name='info-outline' size={25} color={isDark ? '#b0b3b5' : '#919191'} />
            </TouchableOpacity>
          )}
        >
          <TouchableOpacity onPress={handleContact}>
            <View
              style={{
                paddingVertical: 10,
                paddingLeft: 10,
                paddingRight: 20,
                display: 'flex',
                flexDirection: 'row',
                textAlign: 'center',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={require('./isaac.jpeg')}
                style={{ width: 40, height: 40, borderRadius: 20 }}
              />
              <Text style={{ marginLeft: 10 }}>
                WhatApp Clone
                {'\n'}
                By <Text style={{ fontWeight: 'bold' }}>Isaac Ng</Text>
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderRadius: 5,
              paddingVertical: 6,
              paddingHorizontal: 10,
              maringTop: 5,
              marginBottom: 10,
              marginHorizontal: 10,
              backgroundColor: isDark ? '#00af9b' : '#00bfa5'
            }}
            onPress={handleDonate}
          >
            <Text style={{ textAlign: 'center', color: isDark ? '#131d20' : 'white', fontWeight: 'bold' }}>
              ☕ Buy me a coffee
            </Text>
          </TouchableOpacity>
        </Popover>
      </View>
      <WebView
        useWebKit
        ref={ref}
        onLoad={handleLoad}
        userAgent={userAgent}
        javaScriptEnabledAndroid={true}
        originWhitelist={['*', 'file://*']}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        source={{ uri: 'https://web.whatsapp.com/' }}
        style={{ flex: 1, backgroundColor: 'transparent' }}
      />
    </SafeAreaView>
  );
};

export default App;
